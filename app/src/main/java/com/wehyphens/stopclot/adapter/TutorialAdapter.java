package com.wehyphens.stopclot.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wehyphens.stopclot.fragment.Tutorial;
import com.wehyphens.stopclot.fragment.Tutorial2;
import com.wehyphens.stopclot.fragment.Tutorial3;
import com.wehyphens.stopclot.fragment.Tutorial4;

public class TutorialAdapter extends FragmentStatePagerAdapter {
    private static int fragment_count=4;

    public TutorialAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new Tutorial();
            case 1:
                return new Tutorial2();
            case 2:
                return new Tutorial3();
                case 3:
                    return new Tutorial4();
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return fragment_count;
    }
}
