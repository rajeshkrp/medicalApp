package com.wehyphens.stopclot.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.wehyphens.stopclot.R;


/**
 * Created by User on 4/26/2018.
 */

public class SelectLanguage extends AppCompatActivity {
    Button btn_hindi,btn_english;
    ImageView img_home;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        btn_hindi=findViewById(R.id.btn_hindi);
        btn_english=findViewById(R.id.btn_english);


       // img_home=findViewById(R.id.img_home);
       /* img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SelectLanguage.this, HomeActivity.class));
            }
        });*/
        btn_hindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_hindi.setBackgroundColor(Color.parseColor("#e93232"));
                btn_hindi.setTextColor(Color.WHITE);
                btn_english.setBackgroundColor(Color.parseColor("#808080"));
                btn_english.setTextColor(Color.BLACK);
                btn_english.setTextColor(Color.WHITE);
                startActivity(new Intent(SelectLanguage.this, TutorialActivity.class));
            }
        });

        btn_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_english.setBackgroundColor(Color.parseColor("#e93232"));
                btn_english.setTextColor(Color.WHITE);
                btn_hindi.setBackgroundColor(Color.parseColor("#808080"));
                btn_hindi.setTextColor(Color.BLACK);
                btn_hindi.setTextColor(Color.WHITE);
                startActivity(new Intent(SelectLanguage.this, TutorialActivity.class));


            }
        });


    }
}
