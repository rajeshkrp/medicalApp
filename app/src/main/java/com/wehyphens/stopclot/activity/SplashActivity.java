package com.wehyphens.stopclot.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.wehyphens.stopclot.R;

/**
 * Created by User on 4/25/2018.
 */

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=SplashActivity.this;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(context,SelectLanguage.class);
                startActivity(intent);
                finish();

              /*  if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
                    Intent mainIntent = new Intent(SplashActivity.this, HomeLoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, UserHomeActivity.class);
                    startActivity(mainIntent);
                    finish();
                }*/


            }
        }, SPLASH_TIME_OUT);


       // finish();
    }
}
