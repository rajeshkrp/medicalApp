package com.wehyphens.stopclot.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import com.wehyphens.stopclot.R;

import java.util.Calendar;

/**
 * Created by User on 4/25/2018.
 */

public class SchedulerActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner spinner_inrValue;
    TextView tv_day1_minus, tv_day2_minus, tv_day3_minus, tv_day4_minus, tv_day5_minus, tv_day6_minus, tv_day7_minus;
    TextView tv_count_day1, tv_count_day2, tv_count_day3, tv_count_day4, tv_count_day5, tv_count_day6, tv_count_day7;
    TextView tv_day1_plus, tv_day2_plus, tv_day3_plus, tv_day4_plus, tv_day5_plus, tv_day6_plus, tv_day7_plus;
    ImageView img_date;
    TextView tv_date;
    Context context;
    String inrValue = "Last INR value";
    ImageView img_home;
    String[] inrList = new String[]{"Target range of INR", "Target range of INR", "Target therepeutic range of INR"};
static int copunterDay1=0;
static int copunterDay2=0;
static int copunterDay3=0;
static int copunterDay4=0;
static int copunterDay5=0;
static int copunterDay6=0;
static int copunterDay7=0;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schdular);
        initializeViews();
        context = SchedulerActivity.this;
        img_home=findViewById(R.id.img_home);

        tv_day1_minus.setOnClickListener(this);
        tv_day2_minus.setOnClickListener(this);
        tv_day3_minus.setOnClickListener(this);
        tv_day4_minus.setOnClickListener(this);
        tv_day5_minus.setOnClickListener(this);
        tv_day6_minus.setOnClickListener(this);
        tv_day7_minus.setOnClickListener(this);

        tv_day1_plus.setOnClickListener(this);
        tv_day2_plus.setOnClickListener(this);
        tv_day3_plus.setOnClickListener(this);
        tv_day4_plus.setOnClickListener(this);
        tv_day5_plus.setOnClickListener(this);
        tv_day6_plus.setOnClickListener(this);
        tv_day7_plus.setOnClickListener(this);

        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SchedulerActivity.this, HomeActivity.class));
            }
        });


        ArrayAdapter<String> adapterInrValue = new ArrayAdapter(this, android.R.layout.simple_spinner_item, inrList);
        adapterInrValue.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_inrValue.setAdapter(adapterInrValue);
        if (inrValue != null) {
            int spinnerPosition = adapterInrValue.getPosition(inrValue);
            spinner_inrValue.setSelection(spinnerPosition);
        }

        img_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker();
            }
        });


    }

    private void initializeViews() {
        spinner_inrValue = findViewById(R.id.spinner_inrValue);
        img_date = findViewById(R.id.img_date);
        tv_date = findViewById(R.id.tv_date);
        tv_day1_minus = findViewById(R.id.tv_day1_minus);
        tv_day2_minus = findViewById(R.id.tv_day2_minus);
        tv_day3_minus = findViewById(R.id.tv_day3_minus);
        tv_day4_minus = findViewById(R.id.tv_day4_minus);
        tv_day5_minus = findViewById(R.id.tv_day5_minus);
        tv_day6_minus = findViewById(R.id.tv_day6_minus);
        tv_day7_minus = findViewById(R.id.tv_day7_minus);

        tv_count_day1 = findViewById(R.id.tv_count_day1);
        tv_count_day2 = findViewById(R.id.tv_count_day2);
        tv_count_day3 = findViewById(R.id.tv_count_day3);
        tv_count_day4 = findViewById(R.id.tv_count_day4);
        tv_count_day5 = findViewById(R.id.tv_count_day5);
        tv_count_day6 = findViewById(R.id.tv_count_day6);
        tv_count_day7 = findViewById(R.id.tv_count_day7);

        tv_day1_plus = findViewById(R.id.tv_day1_plus);
        tv_day2_plus = findViewById(R.id.tv_day2_plus);
        tv_day3_plus = findViewById(R.id.tv_day3_plus);
        tv_day4_plus = findViewById(R.id.tv_day4_plus);
        tv_day5_plus = findViewById(R.id.tv_day5_plus);
        tv_day6_plus = findViewById(R.id.tv_day6_plus);
        tv_day7_plus = findViewById(R.id.tv_day7_plus);

        tv_count_day1.setText("1");
        tv_count_day2.setText("2");
        tv_count_day3.setText("3");
        tv_count_day4.setText("4");
        tv_count_day5.setText("5");
        tv_count_day6.setText("6");
        tv_count_day7.setText("7");

    }

    private void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                tv_date.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);

                //    selectedFromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_day1_minus:
                if(copunterDay1>0){
                    copunterDay1--;
                }else {
                    copunterDay1=0;
                }
                tv_count_day1.setText(Integer.toString(copunterDay1));
                //Toast.makeText(context, "------>"+copunterDay1, Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_day1_plus:
               if(copunterDay1<10){
                   copunterDay1++;
               }else {
                   copunterDay1=10;
               }
                tv_count_day1.setText(Integer.toString(copunterDay1));
               // Toast.makeText(context, "------>"+copunterDay1, Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_day2_minus:
                if(copunterDay2>0){
                    copunterDay2--;
                }else {
                    copunterDay2=0;
                }
                tv_count_day2.setText(Integer.toString(copunterDay2));

                break;
            case R.id.tv_day2_plus:
                if(copunterDay2<10){
                    copunterDay2++;
                }else {
                    copunterDay2=10;
                }
                tv_count_day2.setText(Integer.toString(copunterDay2));
                break;

            case R.id.tv_day3_minus:
                if(copunterDay3>0){
                    copunterDay3--;
                }else {
                    copunterDay3=0;
                }
                tv_count_day3.setText(Integer.toString(copunterDay3));

                break;
            case R.id.tv_day3_plus:
                if(copunterDay3<10){
                    copunterDay3++;
                }else {
                    copunterDay3=10;
                }
                tv_count_day3.setText(Integer.toString(copunterDay3));
                break;

            case R.id.tv_day4_minus:
                if(copunterDay4>0){
                    copunterDay4--;
                }else {
                    copunterDay4=0;
                }
                tv_count_day4.setText(Integer.toString(copunterDay4));

                break;
            case R.id.tv_day4_plus:
                if(copunterDay4<10){
                    copunterDay4++;
                }else {
                    copunterDay4=10;
                }
                tv_count_day4.setText(Integer.toString(copunterDay4));
                break;

            case R.id.tv_day5_minus:
                if(copunterDay5>0){
                    copunterDay5--;
                }else {
                    copunterDay5=0;
                }
                tv_count_day5.setText(Integer.toString(copunterDay5));

                break;
            case R.id.tv_day5_plus:
                if(copunterDay5<10){
                    copunterDay5++;
                }else {
                    copunterDay5=10;
                }
                tv_count_day5.setText(Integer.toString(copunterDay5));
                break;

            case R.id.tv_day6_minus:
                if(copunterDay6>0){
                    copunterDay6--;
                }else {
                    copunterDay6=0;
                }
                tv_count_day6.setText(Integer.toString(copunterDay6));

                break;
            case R.id.tv_day6_plus:
                if(copunterDay6<10){
                    copunterDay6++;
                }else {
                    copunterDay6=10;
                }
                tv_count_day6.setText(Integer.toString(copunterDay6));
                break;


            case R.id.tv_day7_minus:
                if(copunterDay7>0){
                    copunterDay7--;
                }else {
                    copunterDay7=0;
                }
                tv_count_day7.setText(Integer.toString(copunterDay7));

                break;
            case R.id.tv_day7_plus:
                if(copunterDay7<10){
                    copunterDay7++;
                }else {
                    copunterDay7=10;
                }
                tv_count_day7.setText(Integer.toString(copunterDay7));
                break;

        }
    }
}
