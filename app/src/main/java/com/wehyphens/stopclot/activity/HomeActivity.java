package com.wehyphens.stopclot.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.wehyphens.stopclot.R;

public class HomeActivity extends AppCompatActivity {
    RelativeLayout rel_scheduler_viewer,rel_inr_scheduler;
    String[] lang = new String[]{"English","Hindi"};
    Spinner spinner_Lang;
int i =100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        spinner_Lang =findViewById(R.id.spinner_Lang);
        rel_inr_scheduler=findViewById(R.id.rel_inr_scheduler);
        rel_scheduler_viewer=findViewById(R.id.rel_scheduler_viewer);
        ArrayAdapter<String> adapter =new  ArrayAdapter(this,  android.R.layout.simple_spinner_item,lang);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Lang.setAdapter(adapter);
        spinner_Lang.setDropDownVerticalOffset(80);

        rel_scheduler_viewer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SchedulerActivity.class));
            }
        });

        rel_inr_scheduler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, GraphActivity.class));
            }
        });

    }
}
