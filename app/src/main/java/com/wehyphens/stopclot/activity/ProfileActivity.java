package com.wehyphens.stopclot.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.wehyphens.stopclot.R;
import java.util.Calendar;

/**
 * Created by User on 4/25/2018.
 */

public class ProfileActivity extends AppCompatActivity {
    DatePickerDialog datePickerDialog;
    Context context;
    TextView date;
    ImageView img_home;
    ImageView calender;
    Spinner spinner_inr,spinner_inrValue,quantity;
    String  inrValue="Last INR value";
    String compareValue = "Target therepeutic range of INR";
    String[] inrList=new String[]{"Target therepeutic range of INR","Target therepeutic range of INR","Target therepeutic range of INR"};
   String[] inrValueList=new String[]{"Last INR value","100","200","300","400","500"};

    String age[]={"18","19","20","21","22","26","27","28","29","30"};



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context=ProfileActivity.this;
        date=findViewById(R.id.tv_date);
        calender=findViewById(R.id.img_date);
        spinner_inr=findViewById(R.id.spinner_inr);
        spinner_inrValue=findViewById(R.id.spinner_inrValue);
        quantity=findViewById(R.id.quantity);
        img_home=findViewById(R.id.img_home);


        ArrayAdapter<String> adapterAge= new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, age);
        adapterAge.setDropDownViewResource(android.R.layout.simple_list_item_1);
        quantity.setAdapter(adapterAge);
        quantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setGravity(Gravity.CENTER);
                ((TextView) adapterView.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this,HomeActivity.class));
            }
        });

        ArrayAdapter<String> adapter =new  ArrayAdapter(this,  android.R.layout.simple_spinner_item,inrList){
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                if (position % 2 == 0) { // we're on an even row

                    view.setBackgroundColor(Color.WHITE);

                } else {
                    view.setBackgroundColor(Color.parseColor("#80bebebe"));
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_inr.setAdapter(adapter);
        if (compareValue != null) {
            int spinnerPosition = adapter.getPosition(compareValue);
            spinner_inr.setSelection(spinnerPosition);
        }


        ArrayAdapter<String> adapterInrValue =new  ArrayAdapter(this,  android.R.layout.simple_spinner_item,inrValueList){
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                if (position % 2 == 0) { // we're on an even row

                    view.setBackgroundColor(Color.WHITE);

                } else {
                    view.setBackgroundColor(Color.parseColor("#80bebebe"));
                }
                return view;
            }
        };
       // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_inrValue.setAdapter(adapterInrValue);
        if (inrValue != null) {
            int spinnerPosition = adapter.getPosition(inrValue);
            spinner_inrValue.setSelection(spinnerPosition);

        }

        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker();
            }
        });

    }

    private void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                date.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);

            //    selectedFromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

}
