package com.wehyphens.stopclot.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wehyphens.stopclot.R;
import com.wehyphens.stopclot.activity.LetterSpacingTextView;


public class Tutorial extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tutorial,container,false);

        LetterSpacingTextView textView = new LetterSpacingTextView(getActivity());
        textView.setSpacing(20); //Or any float. To reset to normal, use 0 or LetterSpacingTextView.Spacing.NORMAL

//Add the textView in a layout, for instance:
        ((LinearLayout) view.findViewById(R.id.linear)).addView(textView);
        return view;
    }
}
